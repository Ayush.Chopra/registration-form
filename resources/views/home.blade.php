<!DOCTYPE html>
<html>
<head>
	<title>Register Yourself</title>
	{{ Html::style('css/bootstrap.css') }}
	{{ Html::style('css/jquery-ui.css') }}
	{{ Html::style('css/style.css') }}
	
	
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Registration Form</a>
    </div>
  </div>
</nav>
<div class="jumbotron">

<form class="form-horizontal" method="POST" action="{{ url('/register') }}" enctype="multipart/form-data" >
  <fieldset>
    <legend></legend>
    @if(count($errors) > 0)
    <span class="col-md-10 alert alert-danger">Some fields are either not filled or wrong information is provided.</span>
    	
    @endif
    @if(session('response'))
    	 <span class="col-md-10 alert alert-success">{{ session('response') }}</span>

    @endif
   


<div class="form-group" style="margin-left:25%;">
     	<!-- <span id="user_image"></span> -->


   	 	<img id="img" src="" style="width: 100px; height: 100px;margin-left:12%;margin-bottom:2%; border-radius: 50%;">

   	 	<input style="display: none;" type="file" name="photo" id="photo" >
   	 	<input type="button" name="" value="Choose file" id="browse_file" class="btn btn-primary form-control" style="width:20%;">
   	 	
   	
   	 	<input  type="hidden" name="_token" value="{{ csrf_token() }}">
     	 	
   	 </div>
    
    <div class="form-group">
      <label for="inputFirstName" class="col-lg-2 control-label">First Name</label>
      <div class="col-sm-6" style="margin-bottom: 0px;">
        <input type="text" name="firstName" class="form-control" id="inputFirstName" placeholder="First Name">
      </div>
    </div>

    <div class="form-group">
      <label for="inputLastName" class="col-lg-2 control-label">Last Name</label>
      <div class="col-sm-6">
        <input type="text" name="lastName" class="form-control" id="inputLastName" placeholder="Last Name">
      </div>
    </div>

    

    <div class="form-group">
      <label for="inputEmail" class="col-lg-2 control-label">Email</label>
      <input  type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="col-sm-6">
        <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Email">
      </div>
    </div>
   
    <div class="form-group">
      <label class="col-lg-2 control-label">Gender</label>
      <div class="col-lg-10">
        <div class="radio">
          <label>
            <input type="radio" name="gender" id="optionsRadios1" value="option1" checked="">
            Male
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" name="gender" id="optionsRadios2" value="option2">
            Female
          </label>
        </div>
      </div>
    </div>
     <div class="form-group">
     <label for="datetimepicker1" class="col-lg-2 control-label">DOB</label>
                <div class='input-group date col-lg-6' >
                    <input type='text' name="dob" class="form-control" id='datetimepicker1' />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>


    <div class="form-group">
      <label for="select" class="col-lg-2 control-label">Nationality</label>
      <div class="col-lg-6">
        <select class="form-control" name="nationality" id="select">
          <option value="indian">Indian</option>
          <option value="others">Others</option>
          
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="inputState" class="col-lg-2 control-label">State</label>
      <div class="col-sm-6">
        <input type="text" name="state" class="form-control" id="inputState" placeholder="State">
      </div>
    </div>
    <div class="form-group">
      <label for="inputCity" class="col-lg-2 control-label">City</label>
      <div class="col-sm-6">
        <input type="text" name="city" class="form-control" id="inputCity" placeholder="City">
      </div>
    </div>

    <div class="form-group">
      <div class="col-lg-10 col-lg-offset-2">
      <button type="submit" class="btn btn-primary">Submit</button>
        <button type="reset" class="btn btn-default">Cancel</button>
        
      </div>
    </div>
  </fieldset>
</form>
</div>


<script type="text/javascript" src="{{ url('js/tether.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/jquery-3.2.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/jquery-ui.js') }}"></script>	
	<script type="text/javascript" src="{{ url('js/jquery-ui-datepicker.js') }}"></script>
	<script type="text/javascript" src="{{ url('js/bootstrap.js') }}"></script>		
	

	 <script>
	 $('#browse_file').on('click', function(e){
	 	$('#photo').click();

	 });
	 $('#photo').on('change', function(e){
	 	var fileInput = this;
	 	if (fileInput.files[0])
	 	{
	 		var reader = new FileReader();
	 		reader.onload = function(e)
	 		{
	 			$('#img').attr('src',e.target.result);
	 		}
	 		reader.readAsDataURL(fileInput.files[0]);
	 	}
	 });
	
  	$( function() {
   	 $( "#datetimepicker1" ).datepicker();
  	});

  </script>
</body>
</html>