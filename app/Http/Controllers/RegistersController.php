<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use Input;

class RegistersController extends Controller
{
    public function register(Request $request)
    {
    	$this->validate($request, [
    			
    			'firstName'=> 'required',
    			'lastName'=> 'required',
    			'email'=> 'required',
    			'gender'=> 'required',
    			'dob'=> 'required',
    			'state'=> 'required',
    			'city'=> 'required',
    		]);
    	$name = "";
    	$file = $request->photo;
    	if ($request->hasFile('photo'))
    	{
    		$path =  public_path()."/uploads/avatars/";
    		$name = $file->getClientOriginalName();
    		$extension = $file->getClientOriginalExtension();
    		$name = $name.".".date('y-m-d').".".time().'.'.$extension;
    		$file->move($path,$name);
    		
    	}
    	else
    	{
    		echo "no";
    	}
    	
    	$candidates = new Candidate;

    	$candidates->avatar = $name;    	
    	$candidates->firstName = $request->input('firstName');
    	$candidates->lastName = $request->input('lastName');
    	$candidates->email = $request->input('email');
    	$candidates->gender = $request->input('gender');
    	$candidates->dob = $request->input('dob');
    	$candidates->nationality = $request->input('nationality');
    	$candidates->state = $request->input('state');
    	$candidates->city = $request->input('city');
    	$candidates->save();
    	return redirect('/')->with('response', 'Register Successfully');
    }
}
